<?php

namespace Drupal\whatsapp_cloud_api;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for quick_notification.
 */
class QuickNotificationTranslationHandler extends ContentTranslationHandler {

}
