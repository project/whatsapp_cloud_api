<?php

namespace Drupal\whatsapp_cloud_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Defines a form that configures whatsapp_cloud_api settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * entity Bundle Info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected $bundleInfo;


  /**
   * Constructs a new SettingsForm object.
   *
   * @param \Drupal\whatsapp_cloud_api\whatsapp_cloud_apiDumperPluginManagerInterface $whatsapp_cloud_api_dumper_manager
   *   whatsapp_cloud_api Dumper Plugin Manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityTypeBundleInfo  $bundleInfo) {
    $this->entityTypeManager = $entityTypeManager;
    $this->bundleInfo = $bundleInfo;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'whatsapp_cloud_api_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'whatsapp_cloud_api.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $config = $this->config('whatsapp_cloud_api.settings');
    $values = $config->getRawData();
    $form['app_id'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('App Id'),
      '#required' => TRUE,
      '#default_value' => isset($values['app_id']) ? $config->get('app_id') : '',
      '#description' => 'Get App Id from App Dashboard'
    );
    $form['app_secret'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('App secret'),
      '#required' => TRUE,
      '#default_value' => isset($values['app_secret']) ? $config->get('app_secret') : '',
      '#description' => 'Get App secret from App Dashboard'
    );
    $form['access_token'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Access token'),
      '#required' => TRUE,
      '#default_value' => isset($values['access_token']) ? $config->get('access_token') : '',
      '#description' => 'Get access token from whatsapp cloud API . https://developers.facebook.com/apps/***************/whatsapp-business/wa-dev-console/?business_id=***********',
      '#maxlength' => 255
    );
    $form['phone_number_id'] = array(
      '#type' => 'tel',
      '#title' => $this->t('Phone number ID'),
      '#required' => TRUE,
      '#default_value' => isset($values['phone_number_id']) ? $config->get('phone_number_id') : '',
      '#description' => 'Get Phone number ID from whatsapp cloud API'
    );
    $form['business_account_id'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Whatsapp Business Account Id'),
      '#required' => TRUE,
      '#default_value' => isset($values['business_account_id']) ? $config->get('business_account_id') : '',
      '#description' => 'Get Business Account Id from whatsapp cloud API'
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('whatsapp_cloud_api.settings');
    if(isset($values['access_token'])) {
      $config->set('access_token', $values['access_token']);
    }
    if(isset($values['app_id'])) {
      $config->set('app_id', $values['app_id']);
    }
    if(isset($values['app_secret'])) {
      $config->set('app_secret', $values['app_secret']);
    }
    if(isset($values['phone_number_id'])) {
      $config->set('phone_number_id', $values['phone_number_id']);
    }
    if(isset($values['business_account_id'])) {
      $config->set('business_account_id', $values['business_account_id']);
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
