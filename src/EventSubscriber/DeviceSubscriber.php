<?php

namespace Drupal\whatsapp_cloud_api\EventSubscriber;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use DeviceDetector\DeviceDetector;
use DeviceDetector\ClientHints;

/**
 * Subscribe to KernelEvents::TERMINATE events to recalculate nodes statistics.
 */
class DeviceSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::TERMINATE][] = ['checkMyDevice'];
    return $events;
  }

}
