<?php

namespace Drupal\whatsapp_cloud_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class utitlies.
 */
class WhatsappCloudApiManager extends ControllerBase {
  /**
   * Database Connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Language Manager Instance.
   *
   * @var \Drupal\Core\Database\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs an AliasManager.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(Connection $connection, LanguageManagerInterface $language_manager) {
    $this->database = $connection;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('language_manager'),
    );
  }

  /**
   * Send the OTP.
   *
   * @param string $message
   *   User message.
   * @param string $phone
   *   User phoneNumber.
   */
  public function updateStatus($qnid, $status) {
    $status = [];
    try {

    }
    catch (Exception $e) {
      $message = $e->getMessage();
      \Drupal::logger('whatsapp_message')->notice($message);
    }
    return new JsonResponse($status);
  }


  /**
   * Send the OTP.
   *
   * @param string $message
   *   User message.
   * @param string $phone
   *   User phoneNumber.
   */
  public function handleWebhooks() {
    $status = [];
    try {

    }
    catch (Exception $e) {
      $message = $e->getMessage();
      \Drupal::logger('whatsapp_message')->notice($message);
    }
    return new JsonResponse($status);
  }

  /**
   * Handle webhook.
   */
  public function handle(Request $request) {
    $settings = \Drupal::config('whatsapp_cloud_api.settings');
    if ($request->getMethod() === 'POST' ) {
      $appSecret = $settings->get('app_secret');
      $hash='sha256='.hash('sha256', $appSecret);
      if($request->headers->has('X-Hub-Signature-256')) {
        $signature = $request->headers->get('X-Hub-Signature-256');
        if($signature == $hash) {
          return new Response($signature, Response::HTTP_OK);    
        }
      }
    }
    else {
      if(isset($_GET['hub_verify_token'])) {
        $verifyToken = $settings->get('verify_token');
        if($_GET['hub_verify_token'] == $verifyToken) {
          if(isset($_GET['hub_challenge'])) {
            return new Response($_GET['hub_challenge'], Response::HTTP_OK);
          }
        }
      }
    }
    return new Response('OK', Response::HTTP_OK);
  }

}
