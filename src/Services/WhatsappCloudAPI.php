<?php

namespace Drupal\whatsapp_cloud_api\Services;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Config\ConfigFactory;
use Drupal\field\Entity\FieldConfig;
use GuzzleHttp\ClientInterface;

/**
 *
 */
class WhatsappCloudAPI extends ControllerBase {
  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * @var Whatsapp Api Url
   */
  protected $apiUrl = 'https://graph.facebook.com/v14.0/';

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $languageManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $fieldStorage;

  /**
   * @var \Drupal\Core\Config\ConfigFactory;
   */
  protected $cf;

  /**
   * @var \Drupal\language\Config\LanguageConfigFactoryOverride
   */
  protected $lcf;

  /**
   * Language Manager Instance.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityManager;

  /**
   * Guzzle\Client instance.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs an AliasManager.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $etManager
   *   The language manager.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   *
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The language manager.
   */
  public function __construct(Connection $connection, LanguageManagerInterface $language_manager, EntityTypeManager $etManager, ConfigFactory $configFactory, ClientInterface $httpClient) {
    $this->database = $connection;
    $this->languageManager = $language_manager;
    $this->fieldStorage = $etManager->getStorage('field_storage_config');
    $this->cf = $configFactory;
    $this->entityManager = $etManager;
    $this->httpClient = $httpClient;
  }

  /**
   *
   */
  public function sendMessage($phoneNumber, $fieldData) {
    $config = $this->cf->getEditable('whatsapp_cloud_api.settings');
    $values = $config->getRawData();
    if(!isset($values['phone_number_id'])){
     return false; 
    }
    $apiUrl = $this->apiUrl . $values['phone_number_id'] .'/messages';
    $data = [
      'messaging_product' => 'whatsapp',
      'to' => $phoneNumber,
    ];
    if(is_array($fieldData) && isset($fieldData['type'])){
      foreach ($fieldData as $key => $value) {
        $data[$key] = $value;
      }
    }
    else {
      $data['type'] = 'template';
      $data['template'] = [
        'name' => 'hello_world',
        'language' => [
          'code' => 'en_US'
        ],
      ];
    }
    $response = $this->httpClient->post($apiUrl, [
      'timeout' => 30,
      'verify' => true,
      'body' => json_encode($data, JSON_FORCE_OBJECT),
      'headers' => [
        'Authorization' => 'Bearer '.$values['access_token'],
        'Content-type' => 'application/json',
      ],
    ])->getBody()->getContents();
    $result = json_decode($response, TRUE);
    return $result;
  }


  /**
   *
   */
  public function getPhoneNumbers() {
    $config = $this->cf->getEditable('whatsapp_cloud_api.settings');
    $values = $config->getRawData();
    if(!isset($values['business_account_id'])){
     return false; 
    }
    $apiUrl = $this->apiUrl . $values['business_account_id'] .'/phone_numbers';
    $request = $this->httpClient->request('GET', $apiUrl.'?access_token='.$values['access_token']);
    $response = $request->getBody()->getContents();
    $result = json_decode($response, TRUE);
    return $result;
  }

}
