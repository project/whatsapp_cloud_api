<?php

namespace Drupal\whatsapp_cloud_api;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a Slider entity.
 *
 * We have this interface so we can join the other interfaces it extends.
 *
 * @ingroup whatsapp_message
 */
interface WhatsappMessageInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
