<?php

namespace Drupal\whatsapp_cloud_api\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\user\EntityOwnerTrait;
use Drupal\user\EntityOwnerInterface;

/**
 * Defines the whatsapp_message entity.
 *
 * @ingroup whatsapp_message
 *
 * The following construct is the actual definition of the entity type which
 * is read and cached. Don't forget to clear cache after changes.
 *
 * @ContentEntityType(
 *   id = "whatsapp_message",
 *   label = @Translation("Whatsapp Message"),
 *   handlers = {
 *     "storage" = "Drupal\Core\Entity\Sql\SqlContentEntityStorage",
 *     "storage_schema" = "Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\whatsapp_cloud_api\Entity\Controller\WhatsappMessageBuilder",
 *     "translation" = "Drupal\whatsapp_cloud_api\WhatsappMessageTranslationHandler",
 *     "form" = {
 *       "default" = "Drupal\whatsapp_cloud_api\Form\WhatsappMessageForm",
 *       "delete" = "Drupal\whatsapp_cloud_api\Form\WhatsappMessageDeleteForm",
 *     },
 *     "access" = "Drupal\whatsapp_cloud_api\WhatsappMessageAccessControlHandler",
 *   },
 *   list_cache_contexts = { "user" },
 *   base_table = "whatsapp_message",
 *   data_table = "whatsapp_message_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer whatsapp_message entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "uid" = "uid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "canonical" = "/whatsapp-message/{whatsapp_message}",
 *     "edit-form" = "/whatsapp-message/{whatsapp_message}/edit",
 *     "delete-form" = "/whatsapp-message/{whatsapp_message}/delete",
 *     "collection" = "/admin/content/whatsapp-message"
 *   },
 *   field_ui_base_route = "entity.whatsapp_message.edit_form",
 * )
 *
 * The 'links' above are defined by their path. For core to find the
 * corresponding route, the route title must follow the correct pattern:
 *
 * entity.<entity_type>.<link_title>
 *
 * Example: 'entity.whatsapp_message.canonical'.
 *
 * See the routing file at whatsapp_message.routing.yml for the
 * corresponding implementation.
 */
class WhatsappMessage extends ContentEntityBase implements EntityOwnerInterface{

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   *
   * When a new entity instance is added, set the user_id entity reference to
   * the current user as the creator of the instance.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }
  
  /**
   * {@inheritdoc}
   *
   * Define the field properties here.
   *
   * Field title, type and size determine the table structure.
   *
   * In addition, we can define how the field and its content can be
   * manipulated in the GUI. The behaviour of the widgets used can be
   * determined here.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Notification entity.'))
      ->setReadOnly(TRUE);

    // Standard field, unique outside of the scope of the current project.
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Notification entity.'))
      ->setReadOnly(TRUE);

    // Title field for the slider.
    // We set display options for the view as well as the form.
    // Users with correct privileges can change the view and edit
    // configuration.
    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the whatsapp_message entity.'))
      ->setTranslatable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -10,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['body'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Description'))
      ->setDescription(t('The description'))
      ->setDefaultValue(NULL)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'basic_string',
        'weight' => -9,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => -9,
        'settings' => [
          'rows' => '5',
          'placeholder' => 'Description',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['receipients'] = BaseFieldDefinition::create('entity_reference')
        ->setLabel(t('Message Receipients'))
        ->setSetting('target_type', 'user')
        ->setSetting('handler', 'whatsapp_message')
        ->setTranslatable(TRUE)
        ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'author',
          'weight' => -7,
        ])
        ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'weight' => -7,
          'settings' => [
            'match_operator' => 'CONTAINS',
            'size' => '60',
            'placeholder' => '',
          ],
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE)
        ->setRequired(TRUE)
        ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);


    $fields['image'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Image'))
      ->setDescription(t('Image For Whatsapp'))
      ->setDefaultValue(NULL)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'image_link',
        'weight' => -7,
      ])
      ->setDisplayOptions('form', [
        'type' => 'image',
        'weight' => -7,
        'settings' => [
          'progress_indicator' => 'throbber',
          'file_extensions' => 'png jpg jpeg gif',
          'title_field_required' => FALSE,
          'alt_field_required' => FALSE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

      $fields['wa_message_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Whatsapp Message Remote Id'))
      ->setDescription(t('Whatsapp message Remote Id.'))
      ->setTranslatable(FALSE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);


    $fields['uid']
      ->setLabel(t('Authored by'))
      ->setDescription(t('The username of the content author.'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => -7,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -7,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);


    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The language code of notification entity.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    if(!$this->get('wa_message_id')->isEmpty()){
      return;
    }
    $receipients = $this->receipients->referencedEntities();
    if(is_array($receipients) && count($receipients) > 0){
      if($receipients[0] instanceof \Drupal\user\Entity\User) {
        if($receipients[0]->hasField('field_whatsapp_capi_number') && !$receipients[0]->get('field_whatsapp_capi_number')->isEmpty()){
          $whatsapp = $receipients[0]->field_whatsapp_capi_number->value;
          $whatsappApi = \Drupal::service('whatsapp_cloud_api');
          $fieldData = $this->prepareTextMessage();
          $result = $whatsappApi->sendMessage($whatsapp,$fieldData);
          if(isset($result['messages'])){
            $this->set('wa_message_id',$result['messages'][0]['id']);
          }
        }
      }
    }
  }

  public function formatMessageFields(){
    $fieldData = [];
    $fieldData['type'] = 'template';
    $fieldData["recipient_type"] = "individual";
    $fieldData['template'] = [
      'name' => 'test_message',
      'language' => [
        'code' => 'en'
      ],
    ];
    $fieldData['template']['components'][] = [
      'type' => 'header',
      'parameters' => [
        [
          'type' => 'text',
          'text' => $this->getTitle()
        ]
      ]
    ];
    $fieldData['template']['components'][] = [
      'type' => 'body',
      'parameters' => [
        [
          'type' => 'text',
          'text' => $this->body->value
        ]
      ]
    ];
    return $fieldData;
  }

  public function prepareTextMessage(){
    $fieldData = [];
    $fieldData['type'] = 'text';
    $content = '*'.$this->getTitle()."*\n";
    $content .= $this->body->value;
    $fieldData['text']['body'] = $content;
    return $fieldData;
  }

  public function prepareMediaMessage(){
    $fieldData = [];
    return $fieldData;
  }

}
